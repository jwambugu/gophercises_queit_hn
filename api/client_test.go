package api

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestClient_defaultify(t *testing.T) {
	var c Client
	c.defaultify()

	if c.baseURL != apiBaseURL {
		t.Errorf("c.baseURL: want %s, got %s", apiBaseURL, c.baseURL)
	}
}

func setup()(string, func()) {
	mux := http.NewServeMux()

	mux.HandleFunc("/topstories.json", func(w http.ResponseWriter, r *http.Request) {
		d, err := json.Marshal([]int{0, 1, 2, 3, 4})

		if err != nil {
			log.Fatal(d)
		}

		_, _ = fmt.Fprint(w, string(d))
	})

	mux.HandleFunc("/item/", func(w http.ResponseWriter, r *http.Request) {
		d, err := json.Marshal(Item{
			By:          "Jay",
			Descendants: 1,
			ID:          1,
			Kids:        []int{24171581,24171330},
			Score:       1,
			Time:        1597509787,
			Title:       "Test Story Title",
			Type:        "story",
			Text:        "",
			URL:         "https://www.test-story.com",
		})

		if err != nil {
			log.Fatal(d)
		}


		_, _ = fmt.Fprint(w, string(d))
	})

	server := httptest.NewServer(mux)

	return server.URL, func() {
		server.Close()
	}
}

func TestClient_TopItems(t *testing.T) {
	baseURL, teardown := setup()

	defer teardown()

	c := Client{
		baseURL: baseURL,
	}

	ids, err := c.TopItems()

	if err != nil {
		t.Errorf("client.TopItems() recieved an error: %s", err.Error())
	}

	if len(ids) != 5 {
		t.Errorf("len(ids): want %d got %d", 5, len(ids))
	}
}

func TestClient_GetItem(t *testing.T) {
	baseURL, teardown := setup()
	defer teardown()

	c := Client{
		baseURL: baseURL,
	}

	item, err := c.GetItem(1)
	if err != nil {
		t.Errorf("client.GetItem() received an error: %s", err.Error())
	}
	// If this stuff errors it means our JSON is incorrect, which is unlikely, so
	// we can just check one field and consider that enough
	if item.By != "Jay" {
		t.Errorf("item.By: want %s, got %s", "test_user", item.By)
	}
}
