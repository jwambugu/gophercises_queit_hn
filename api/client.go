package api

import (
	"encoding/json"
	"fmt"
	"net/http"
)

const (
	apiBaseURL = "https://hacker-news.firebaseio.com/v0"
)

type (
	// Client is an API client used to interact with the Hacker News API
	Client struct {
		baseURL string
	}

	//  Item represents a single item returned by the HN API.
	Item struct {
		By          string `json:"by"`
		Descendants int    `json:"descendants"`
		ID          int    `json:"id"`
		Kids        []int  `json:"kids"`
		Score       int    `json:"score"`
		Time        int    `json:"time"`
		Title       string `json:"title"`
		Type        string `json:"type"`

		// Only one of these should exist
		Text string `json:"text"`
		URL  string `json:"url"`
	}
)

func (c *Client) defaultify() {
	if c.baseURL == "" {
		c.baseURL = apiBaseURL
	}
}

// TopItems gets the most recent to items from the API
func (c *Client) TopItems() ([]int, error) {
	// Client a default client if none exists
	c.defaultify()

	response, err := http.Get(fmt.Sprintf("%s/topstories.json", c.baseURL))

	if err != nil {
		return nil, err
	}

	defer response.Body.Close()

	var ids []int

	if err := json.NewDecoder(response.Body).Decode(&ids); err != nil {
		return nil, err
	}

	return ids, nil
}

// GetItem will return the Item defined by the provided ID.
func (c *Client) GetItem(id int) (Item, error) {
	// Client a default client if none exists
	c.defaultify()

	var item Item

	response, err := http.Get(fmt.Sprintf("%s/item/%d.json", c.baseURL, id))

	if err != nil {
		return item, err
	}

	defer response.Body.Close()

	if err := json.NewDecoder(response.Body).Decode(&item); err != nil {
		return item, err
	}

	return item, nil
}
