package main

import (
	"errors"
	"flag"
	"fmt"
	"gitlab.com/jwambugu/gophercises_queit_hn/api"
	"html/template"
	"log"
	"net/http"
	"net/url"
	"sort"
	"strings"
	"sync"
	"time"
)

type (
	item struct {
		api.Item
		Host string
	}

	templateData struct {
		Stories []item
		Time    time.Duration
	}

	storyCache struct {
		numberOfStories int
		cache           []item
		expiration      time.Time
		duration        time.Duration
		mutex           sync.Mutex
	}
)

func parseStory(i api.Item) item {
	ret := item{
		Item: i,
	}

	parsedURL, err := url.Parse(ret.URL)

	if err == nil {
		ret.Host = strings.TrimPrefix(parsedURL.Hostname(), "www.")
	}

	return ret
}

func isStoryLink(item item) bool {
	return item.Type == "story" && item.URL != ""
}

func getStories(ids []int) []item {
	type result struct {
		index int
		item  item
		err   error
	}

	resultChan := make(chan result)

	for i := 0; i < len(ids); i++ {
		go func(i, id int) {
			var client api.Client

			hnItem, err := client.GetItem(id)

			if err != nil {
				resultChan <- result{
					index: i,
					err:   err,
				}
			}

			resultChan <- result{
				index: i,
				item:  parseStory(hnItem),
			}
		}(i, ids[i])
	}

	var results []result

	for i := 0; i < len(ids); i++ {
		results = append(results, <-resultChan)
	}

	sort.Slice(results, func(i, j int) bool {
		return results[i].index < results[j].index
	})

	var stories []item

	for _, r := range results {
		if r.err != nil {
			continue
		}

		if isStoryLink(r.item) {
			stories = append(stories, r.item)
		}
	}

	return stories
}

func getTopStories(numberOfStories int) ([]item, error) {
	var client api.Client

	// Fetch the top stories from the api
	ids, err := client.TopItems()

	if err != nil {
		return nil, errors.New("failed to load top stories")
	}

	var stories []item

	start := 0

	for len(stories) < numberOfStories {
		storiesToGet := (numberOfStories - len(stories)) * 5 / 4

		stories = append(stories, getStories(ids[start:start+storiesToGet])...)
		start += storiesToGet
	}

	return stories[:numberOfStories], nil
}

func (c *storyCache) stories() ([]item, error) {
	c.mutex.Lock()

	defer c.mutex.Unlock()

	if time.Now().Sub(c.expiration) < 0 {
		return c.cache, nil
	}

	stories, err := getTopStories(c.numberOfStories)

	if err != nil {
		return nil, err
	}

	c.expiration = time.Now().Add(c.duration)
	c.cache = stories
	return c.cache, nil
}

func storiesHandler(numberOfStories int, templ *template.Template) http.HandlerFunc {
	sCache := storyCache{
		numberOfStories: numberOfStories,
		duration:        3 * time.Minute,
	}

	go func() {
		ticker := time.NewTicker(sCache.duration)

		for {
			temporaryCache := storyCache{
				numberOfStories: numberOfStories,
				duration:        3 * time.Minute,
			}

			_, _ = temporaryCache.stories()

			sCache.mutex.Lock()
			sCache.cache = temporaryCache.cache
			sCache.expiration = temporaryCache.expiration
			sCache.mutex.Unlock()

			fmt.Println(sCache.expiration, "new")
			<-ticker.C
		}
	}()

	return func(w http.ResponseWriter, r *http.Request) {
		start := time.Now()

		stories, err := sCache.stories()

		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}

		data := templateData{
			Stories: stories,
			Time:    time.Now().Sub(start),
		}

		err = templ.Execute(w, data)

		if err != nil {
			http.Error(w, "Failed to process the template", http.StatusInternalServerError)
			return
		}
	}
}

func main() {
	// Parse the flags to run the app
	var port, numberOfStories int

	flag.IntVar(&port, "port", 3000, "the port to start the web server on")
	flag.IntVar(&numberOfStories, "num_stories", 30, "the number of stories to display")
	flag.Parse()

	templ := template.Must(template.ParseFiles("./index.gohtml"))

	http.HandleFunc("/", storiesHandler(numberOfStories, templ))

	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", port), nil))
}
